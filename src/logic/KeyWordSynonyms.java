package logic;

import java.util.LinkedHashMap;
import java.util.Map;

public class KeyWordSynonyms {
	private static KeyWordSynonyms instance = null;
	private Map<String, String> synonyms;

	private KeyWordSynonyms(){
	
	synonyms = new LinkedHashMap<String, String>();
	//CARGAR SINONIMOS:
	//NAME:
	synonyms.put("noun", "name");
	synonyms.put("forename", "name");
	synonyms.put("appellation", "name");
	synonyms.put("cognomen", "name");
	synonyms.put("name", "name");
	//MOM:
	synonyms.put("mother", "mom");
	synonyms.put("mum", "mom");
	synonyms.put("mama", "mom");
	synonyms.put("mummy", "mom");
	synonyms.put("momma", "mom");
	synonyms.put("mamma", "mom");
	synonyms.put("ma", "mom");
	synonyms.put("mom", "mom");
	//DAD:
	synonyms.put("father", "dad");
	synonyms.put("daddy", "dad");
	synonyms.put("papa", "dad");
	synonyms.put("pa", "dad");
	synonyms.put("pappy", "dad");
	synonyms.put("governor", "dad");
	synonyms.put("pop", "dad");
	synonyms.put("dad", "dad");
	//FAMILY:
	synonyms.put("blood", "family");
	synonyms.put("clan", "family");
	synonyms.put("folks", "family");
	synonyms.put("house", "family");
	synonyms.put("kin", "family");
	synonyms.put("kindred", "family");
	synonyms.put("kinfolk", "family");
	synonyms.put("kinsfolk", "family");
	synonyms.put("line", "family");
	synonyms.put("linage", "family");
	synonyms.put("race", "family");
	synonyms.put("stock", "family");
	synonyms.put("tribe", "family");
	synonyms.put("family", "family");
	synonyms.put("hosehold", "family");
	synonyms.put("kith", "family");
	synonyms.put("brood", "family");

	//FRIENDS:
	synonyms.put("buddy", "friends");
	synonyms.put("chum", "friends");
	synonyms.put("comrade", "friends");
	synonyms.put("confidant", "friends");
	synonyms.put("confidante", "friends");
	synonyms.put("crony", "friends");
	synonyms.put("intimate", "friends");
	synonyms.put("pal", "friends");
	synonyms.put("friends", "friends");
	synonyms.put("friend", "friends");
	synonyms.put("companion", "friends");
	synonyms.put("fellow", "friends");
	synonyms.put("partner", "friends");
	synonyms.put("peer", "friends");
	synonyms.put("abettor", "friends");
	synonyms.put("accomplice", "friends");
	synonyms.put("confederate", "friends");
	synonyms.put("friendly", "friends");
	//MEMBERS:
	synonyms.put("component", "members");
	synonyms.put("constituent", "members");
	synonyms.put("element", "members");
	synonyms.put("factor", "members");
	synonyms.put("members", "members");
	synonyms.put("member", "members");

	//OLD:
	synonyms.put("aged", "old");
	synonyms.put("aging", "old");
	synonyms.put("ageing", "old");
	synonyms.put("ancient", "old");
	synonyms.put("elderly", "old");
	synonyms.put("long-lived", "old");
	synonyms.put("older", "old");
	synonyms.put("senescent", "old");
	synonyms.put("old", "old");

	//NUMBERS:
	synonyms.put("1", "one");
	synonyms.put("one", "one");
	synonyms.put("2", "two");
	synonyms.put("two", "two");
	synonyms.put("3", "three");
	synonyms.put("three", "three");
	synonyms.put("4", "four");
	synonyms.put("four", "four");
	synonyms.put("5", "five");
	synonyms.put("five", "five");
	synonyms.put("6", "six");
	synonyms.put("six", "six");
	synonyms.put("7", "seven");
	synonyms.put("seven", "seven");
	synonyms.put("8", "eight");
	synonyms.put("eight", "eight");
	synonyms.put("9", "nine");
	synonyms.put("nine", "nine");
	synonyms.put("10", "ten");
	synonyms.put("ten", "ten");
	synonyms.put("11", "eleven");
	synonyms.put("eleven", "eleven");
	synonyms.put("12", "twelve");
	synonyms.put("twelve", "twelve");
	synonyms.put("13", "thirteen");
	synonyms.put("thirteen", "thirteen");
	
	//BORN:
	synonyms.put("congenital", "born");
	synonyms.put("natural", "born");
	synonyms.put("aboriginal", "born");
	synonyms.put("native", "born");
	synonyms.put("born", "born");
	//LIVE:
	synonyms.put("active", "live");
	synonyms.put("alive", "live");
	synonyms.put("functional", "live");
	synonyms.put("functioning", "live");
	synonyms.put("living", "live");
	synonyms.put("live", "live");

	//SCHOOL:
	synonyms.put("schoolhouse", "school");
	synonyms.put("school", "school");
	synonyms.put("highschool", "school");
	synonyms.put("college", "school");
	synonyms.put("institute", "school");
	
	//TEACHER
	synonyms.put("educator", "teacher");
	synonyms.put("instructor", "teacher");
	synonyms.put("pedagogue", "teacher");
	synonyms.put("preceptor", "teacher");
	synonyms.put("schoolteacher", "teacher");
	synonyms.put("professor", "teacher");
	synonyms.put("teacher", "teacher");
	synonyms.put("teachers", "teacher");

	//BANK:
	synonyms.put("bank", "bank");
	synonyms.put("banks", "bank");
	synonyms.put("fund", "bank");
	synonyms.put("stock", "bank");
	synonyms.put("savings", "bank");
	
	//WORK:
	synonyms.put("business", "work");
	synonyms.put("role", "work");
	synonyms.put("task", "work");
	synonyms.put("function", "work");
	synonyms.put("work", "work");
	synonyms.put("works", "work");
	synonyms.put("worked", "work");
	synonyms.put("working", "work");
	
	//JOB
	synonyms.put("job","job");
	synonyms.put("jobs","job");
	synonyms.put("profession","job");
	synonyms.put("occupation","job");

	//MONEY:
	synonyms.put("buks", "money");
	synonyms.put("cash", "money");
	synonyms.put("coin", "money");
	synonyms.put("currency", "money");
	synonyms.put("dough", "money");
	synonyms.put("gold", "money");
	synonyms.put("pelf", "money");
	synonyms.put("wampum", "money");
	synonyms.put("money", "money");
	
	//SAVE:
	synonyms.put("economize", "save");
	synonyms.put("pinch", "save");
	synonyms.put("scrimp", "save");
	synonyms.put("skimp", "save");
	synonyms.put("spare", "save");
	synonyms.put("preserve", "save");
	synonyms.put("conserve", "save");
	synonyms.put("save", "save");
	//BUY:
	synonyms.put("pick up", "buy");
	synonyms.put("purchase", "buy");
	synonyms.put("take", "buy");
	synonyms.put("acquire", "buy");
	synonyms.put("get", "buy");
	synonyms.put("obtain", "buy");
	synonyms.put("procure", "buy");
	synonyms.put("pay for", "buy");
	synonyms.put("trade for", "buy");
	synonyms.put("buy", "buy");

	//JEWELRY:
	synonyms.put("jewel", "jewelry");
	synonyms.put("gem", "jewelry");
	synonyms.put("gemstone", "jewelry");
	synonyms.put("jewelry", "jewelry");
	synonyms.put("necklace", "jewelry");
	synonyms.put("collar", "jewelry");
	synonyms.put("choker", "jewelry");

	//SPARKLY:
	synonyms.put("sparkly", "sparkly");

	//DIAMOND:
	synonyms.put("jewel", "diamond");
	synonyms.put("gem", "diamond");
	synonyms.put("diamond", "diamond");

	//HOME:
	synonyms.put("abode", "home");
	synonyms.put("domicile", "home");
	synonyms.put("dwelling", "home");
	synonyms.put("fireside", "home");
	synonyms.put("residence", "home");
	synonyms.put("place", "home");
	synonyms.put("roof", "home");
	synonyms.put("home", "home");

	//DECORATION:
	synonyms.put("adornment", "decoration");
	synonyms.put("beautifier", "decoration");
	synonyms.put("embellisher", "decoration");
	synonyms.put("ornament", "decoration");
	synonyms.put("ornamentation", "decoration");
	synonyms.put("garnish", "decoration");
	synonyms.put("garniture", "decoration");
	synonyms.put("decoration", "decoration");

	//DRAWING:
	synonyms.put("sketch", "drawing");
	synonyms.put("desing", "drawing");
	synonyms.put("drawing", "drawing");
	
	//BUILD:
	synonyms.put("construct", "build");
	synonyms.put("build", "build");
	synonyms.put("built", "build");

	//VACATION:
	synonyms.put("break", "vacation");
	synonyms.put("leave", "vacation");
	synonyms.put("recess", "vacation");
	synonyms.put("holidays", "vacation");
	synonyms.put("vacation", "vacation");
	//TOWN:
	synonyms.put("burg", "town");
	synonyms.put("village", "town");
	synonyms.put("city", "town");
	synonyms.put("cosmopolis", "town");
	synonyms.put("megalopolis", "town");
	synonyms.put("metropolis", "town");
	synonyms.put("municipality", "town");
	synonyms.put("town", "town");

	//LICENSE:
	synonyms.put("license", "license");
	synonyms.put("permit", "license");

	//PET:
	synonyms.put("mascot", "pet");
	synonyms.put("pet", "pet");

	//HOBBY:
	synonyms.put("craft", "hobby");
	synonyms.put("occupation", "hobby");
	synonyms.put("sport", "hobby");	
	synonyms.put("hobbies", "hobby");
	synonyms.put("hobby", "hobby");
	
	//TOBE
	synonyms.put("be", "toBe");
	synonyms.put("am", "toBe");
	synonyms.put("are", "toBe");
	synonyms.put("is", "toBe");
	synonyms.put("was", "toBe");
	synonyms.put("were", "toBe");
	synonyms.put("being", "toBe");
	synonyms.put("been", "toBe");
	
	//SU
//	synonyms.put("his", "su");
//	synonyms.put("her", "su");
//	synonyms.put("their", "su");
//	synonyms.put("your", "su");
//	synonyms.put("our", "su");
	
	//HOUSE
	synonyms.put("house", "house");
	synonyms.put("houses", "house");
	synonyms.put("treehouse", "house");
	synonyms.put("apartment", "house");
	synonyms.put("building", "house");
	synonyms.put("condo", "house");

	//DRIVE
	synonyms.put("drive", "drive");
	synonyms.put("driving", "drive");
	
	//ACTIVITY
	synonyms.put("activity", "activity");
	synonyms.put("activities", "activity");
	synonyms.put("dedication", "activity");

	//WHAT
	synonyms.put("what", "what");

	//YOUR
	synonyms.put("your", "your");
	synonyms.put("his", "your");
	synonyms.put("her", "your");
	synonyms.put("their", "your");
	synonyms.put("our", "your");
	
	//IN
	synonyms.put("in", "in");
	
	//GRADE
	synonyms.put("grade", "grade");
	synonyms.put("level", "grade");
	
	//KINDERGARTEN
	synonyms.put("kindergarten", "kindergarten");
	synonyms.put("kinder", "kindergarten");
	
	//LIKE
	synonyms.put("like", "like");
	synonyms.put("likes", "like");
	synonyms.put("liked", "like");
	synonyms.put("liking", "like");
	
	//CLASS
	synonyms.put("class", "class");
	
	//PIGGY
	synonyms.put("piggy", "piggy");
	
	//SAVE
	synonyms.put("save", "save");
	synonyms.put("economize", "save");
	synonyms.put("pinch", "save");
	synonyms.put("scrimp", "save");
	synonyms.put("skimp", "save");
	synonyms.put("spare", "save");
	synonyms.put("preserve", "save");
	synonyms.put("conserve", "save");

	//BUY
	synonyms.put("buy", "buy");
	synonyms.put("bought", "buy");
	synonyms.put("buying", "buy");
	synonyms.put("acquisition", "buy");
	synonyms.put("purchase", "buy");
	synonyms.put("investment", "buy");

	//YOU
	synonyms.put("you","you");

	

	

}
	
	public static String getKeyword(String synonym) {
		if(instance == null)
			instance = new KeyWordSynonyms();
		return instance.synonyms.get(synonym);
	}
}
