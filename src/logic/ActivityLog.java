package logic;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ActivityLog {
	
	private static ActivityLog instance = null;
	
	private ArrayList<LogEntry> logEntries;
	
	private ActivityLog(){
		logEntries = new ArrayList<LogEntry>();
	}
	
	public static void addToLog(LogEntry le) {
		if(instance == null)
			instance = new ActivityLog();
		instance.logEntries.add(le);
	}
	
	public static void writeLog() {
		
		if(instance == null) {
			instance = new ActivityLog();
		}else {
			try {
				Date date = new Date();
				DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
				DateFormat formatoHora = new SimpleDateFormat("HH-mm-ss");
				FileOutputStream fos = new FileOutputStream("Activitylog-d"+formatoFecha.format(date)+"-h"+formatoHora.format(date)+".txt",false);
				
				for(LogEntry le : instance.logEntries) {
					fos.write(le.toString().getBytes(Charset.forName("UTF-8")));
				}
				
				fos.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
