package logic;
import java.util.ArrayList;
import java.util.List;

public class Rule {
	
	// Structure //
	int id;
	int priority;
	int novelty;
	ArrayList<String> keyWords;
	String response;
	String action;
	
	public Rule(int priority, int novelty, String keyWords, String response, String action) {
		this.id = novelty;
		this.priority = priority;
		this.novelty = novelty;
		this.response = response;
		this.action = action;
		
		this.keyWords = new ArrayList<String>();
		
		String[] splittedKeyWord = keyWords.split(" ");
		
		for(String word : splittedKeyWord) {
			if(word.isEmpty() == false) {
				this.keyWords.add(word);
			}
		}
	}
	
	public boolean isActive(List<String> splittedSentece) {
		
		//String[] splittedSentece = sentece.split(" ");
		
		ArrayList<String> matchedKeyWords = new ArrayList<String>();
		
		for(String word : splittedSentece) {
			if((word.isEmpty() == false) && this.matchesKeyWord(word) && !matchedKeyWords.contains(matchedKeyWords))
				matchedKeyWords.add(word);
		}
		return this.keyWords.size() == matchedKeyWords.size();
	}
	
	private boolean matchesKeyWord(String word) {
		for(String keyWord : keyWords) {
			if(keyWord.equals(word))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj.getClass().equals(this.getClass())) {
			return ((Rule) obj).id == this.id;
		}
		return false;
	}
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		for(String keyw : keyWords) {
			sb.append(", "+keyw);
		}
		
		String kws;
		if(sb.length() > 0) {
			
			kws = sb.toString().substring(2);
		}else {
			kws = "<none>";
		}
		return "Rule n� " + id + " priority " + priority + ":\r\n Keywords: " + kws + "\r\n Agent response: " + response + "\r\n Action: " + action;
	}
	
	public String ruleNumber() {
		return "Rule n� " + id;
	}
}
