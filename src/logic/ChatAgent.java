package logic;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;

public class ChatAgent {

	private static ChatAgent instance=null;
	
	private List<Rule> rules;
	private List<Rule> activeRules;
	private List<Criteria> criteria;
	
	public static ChatAgent getInstance() {
		if(instance == null)
			instance = new ChatAgent();
		return instance;
	}
	
	private ChatAgent() {
		//To DO
		rules = RuleManager.getRules();
		activeRules = new ArrayList<Rule>(); 
		criteria = new ArrayList<Criteria>();
		criteria.add(Criteria.Priority);
		criteria.add(Criteria.Specificity);
		criteria.add(Criteria.Random);
	}
	
	public String getResponse(String inputText) {
		
		LogEntry actualLog= new LogEntry();
		
		actualLog.setInputText(inputText);
		
		List<String> outputPreprocesamiento = this.preprocesar(inputText);
		actualLog.setOutputPreprocesamiento(outputPreprocesamiento);
		
		List<Rule> outputCotejo = this.cotejar(outputPreprocesamiento);
		//System.out.println(outputCotejo.size());
		actualLog.setOutputCotejo(outputCotejo);
		
		actualLog.setCriterio(criteria);
		Rule outputResolucionConflicto = this.resolverConflicto(outputPreprocesamiento, outputCotejo);
		
		if(outputResolucionConflicto == null) {
			actualLog.setOutputResolucionConflicto(outputResolucionConflicto);
			
			ActivityLog.addToLog(actualLog);
			return "";
		}else {
			actualLog.setOutputResolucionConflicto(outputResolucionConflicto);
			
			System.out.println("-----------------------------------------------------------");
			System.out.println(outputResolucionConflicto.toString());
			ActivityLog.addToLog(actualLog);
			return outputResolucionConflicto.response;
		}
		
	}
	
	private ArrayList<String> preprocesar(String inputText) {
		ArrayList<String> keywords = new ArrayList<String>();
		
		String[] splittedInputText = inputText.toLowerCase().split(" |\\.|\\,|\\?|\\!|\\'");
		
		
		for(String word : splittedInputText) {
			if(word.isEmpty() == false) {
				String keyword = null;
				keyword = KeyWordSynonyms.getKeyword(word);
				if(keyword != null) {
					if (keywords.contains(keyword)==false){
					keywords.add(keyword);
					//System.out.println(keyword);
				}
					
			}	
		}
		}
		return keywords;
	}
	
	private List<Rule> cotejar(List<String> outputPreprocesamiento) {
		// TODO Auto-generated method stub
		activeRules = new ArrayList<Rule>();
		
		//System.out.println("There are "+rules.size()+ " rules.");
		
		
		for(Rule rule : rules) {
			if(rule.isActive(outputPreprocesamiento) && (activeRules.contains(rule) == false)) {
				activeRules.add(rule);
			}
		}
		
		return activeRules;
	}
	
	private Rule resolverConflicto(List<String> presentKeywords, List<Rule> outputCotejo) {
		Iterator<Criteria> li= criteria.iterator();
		List<Rule> conflictingRules = outputCotejo;
		while(conflictingRules.size()>1 && li.hasNext() ) {
			Criteria actualCriteria = li.next();
			conflictingRules = this.applyCriteria(presentKeywords, conflictingRules, actualCriteria);
		}
		
		if(conflictingRules.size()>1) {
			conflictingRules = this.applyCriteria(presentKeywords, conflictingRules, Criteria.Random);
		}
		
		if(conflictingRules.size() == 1) {
			return conflictingRules.get(0);
		}else {
			return null;
		}	
	}
	
	private ArrayList<Rule> applyCriteria(List<String> presentKeywords, List<Rule> outputCotejo, Criteria criteria){
		
		ArrayList<Rule> result = new ArrayList<Rule>();
		
		
		switch(criteria) {
		case Specificity:
			System.out.println("Entro specificity");
			int maxNumberOfKeywords = 0;
			for(Rule r : outputCotejo) {
				if(r.keyWords.size() > maxNumberOfKeywords) {
					maxNumberOfKeywords = r.keyWords.size(); 
				}
			}
			
			for(Rule r : outputCotejo) {
				if(r.keyWords.size() == maxNumberOfKeywords) {
					result.add(r);
				}
			}
			break;
		case Novelty:
			int maximunId =0;
			for(Rule r : outputCotejo) {
				if(r.id > maximunId) {
					maximunId = r.id;
				}
			}
			for(Rule r : outputCotejo) {
				if(r.id == maximunId) {
					result.add(r);
				}
			}
			break;
		case Priority:
			int maximunPriority =0;
			for(Rule r : outputCotejo) {
				if(r.priority> maximunPriority) {
					maximunPriority = r.priority;
				}
			}
			for(Rule r : outputCotejo) {
				if(r.priority == maximunPriority) {
					result.add(r);
				}
			}
			break;
		default:
			Random r = new Random();
			r.setSeed(System.currentTimeMillis());
			int index = r.nextInt(outputCotejo.size());
			result.add(outputCotejo.get(index));
			break;	
		}
		return result;
	}
}
