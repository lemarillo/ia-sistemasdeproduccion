package logic;

public enum Criteria {
	Novelty,
	Priority,
	Random,
	Specificity
}
