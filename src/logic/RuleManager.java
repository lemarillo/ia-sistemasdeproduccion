package logic;
import java.util.ArrayList;
import java.util.List;
import java.lang.UnsupportedOperationException;

public class RuleManager {
	
	//public static int id=0;
	
	public static List<Rule> getRules(){
		//To Do
		int id =0;
		ArrayList<Rule> aux = new ArrayList<Rule>();
		//NAME RULES:
		aux.add(new Rule(1,id++,"what name","you shouldn't answer about your name",""));                        //0
		aux.add(new Rule(3,id++,"name mom","you shouldn't answer about family names","Notify parents"));        //1
		aux.add(new Rule(3,id++,"name dad","you shouldn't answer about family names","Notify parents"));        //2
		aux.add(new Rule(2,id++,"name family","you shouldn't answer about family names","Notify parents"));     //3
		aux.add(new Rule(2,id++,"name friends","you shouldn't answer about family names","Notify parents"));    //4
		aux.add(new Rule(2,id++,"name members","you shouldn't answer about family names","Notify parents"));    //5
		//OLD RULES:
		aux.add(new Rule(4,id++,"old you","you mustn't answer about anybody ages","Record conversation"));       //6
		//TO BE RULES:
		aux.add(new Rule(2,id++,"toBe one","you mustn't answer about anybody age","Record conversation"));       //7
		aux.add(new Rule(2,id++,"toBe two","you mustn't answer about anybody age","Record conversation"));       //8
		aux.add(new Rule(2,id++,"toBe three","you mustn't answer about anybody age","Record conversation"));     //9
		aux.add(new Rule(2,id++,"toBe four","you mustn't answer about anybody age","Record conversation"));      //10
		aux.add(new Rule(2,id++,"toBe five","you mustn't answer about anybody age","Record conversation"));      //11
		aux.add(new Rule(2,id++,"toBe six","you mustn't answer about anybody age","Record conversation"));       //12
		aux.add(new Rule(2,id++,"toBe seven","you mustn't answer about anybody age","Record conversation"));     //13
		aux.add(new Rule(2,id++,"toBe eight","you mustn't answer about anybody age","Record conversation"));     //14
		aux.add(new Rule(2,id++,"toBe nine","you mustn't answer about anybody age","Record conversation"));      //15
		aux.add(new Rule(2,id++,"toBe ten","you mustn't answer about anybody age","Record conversation"));       //16
		aux.add(new Rule(2,id++,"toBe eleven","you mustn't answer about anybody age","Record conversation"));	 //17
		aux.add(new Rule(2,id++,"toBe twelve","you mustn't answer about anybody age","Record conversation"));    //18
		aux.add(new Rule(2,id++,"toBe thirteen","you mustn't answer about anybody age","Record conversation"));  //19
		//BORN RULES:
		aux.add(new Rule(4,id++,"born in","...","Notify parents"));                                             //20
		//LIVE RULES:
		aux.add(new Rule(5,id++,"live in","you mustn't answer were you live","notify parents"));        		//21                       
		//SCHOOL RULES:
		aux.add(new Rule(3,id++,"school grade","you mustn't answer about your school","notify parents"));  		//22                         
		aux.add(new Rule(3,id++,"school kindergarten","you mustn't answer you school","notify parents"));		//23
		//TEACHER RULES: 
		aux.add(new Rule(1,id++,"teacher toBe","...","Record conversation"));									//24
		aux.add(new Rule(4,id++,"teacher like","you mustn't answer about your teacher","Record conversation"));	//25			    
		aux.add(new Rule(3,id++,"teacher class","...","Record conversation"));									//26
		//BANK RULES:
		aux.add(new Rule(5,id++,"bank work","you mustn't answer","Notify parents"));							//27
		aux.add(new Rule(5,id++,"bank piggy","...","Record conversation"));                                    	//28
		//MONEY RULES:
		aux.add(new Rule(5,id++,"money","you mustn't answer about money",""));									//29
		aux.add(new Rule(5,id++,"money save","you mustn't answer about money",""));                             //30
		aux.add(new Rule(5,id++,"money buy","you mustn't answer about money","Notify parents"));				//31
		//JEWELRY RULES:	
		aux.add(new Rule(5,id++,"jewelry mom","you mustn't answer"," "));										//32							
		aux.add(new Rule(5,id++,"jewelry dad","you mustn't answer"," "));										//33
		aux.add(new Rule(5,id++,"jewelry family","you mustn't answer"," "));									//34
		aux.add(new Rule(5,id++,"jewelry sparkly","you mustn't answer"," "));                                   //35
		aux.add(new Rule(5,id++,"jewelry diamond","you mustn't answer"," "));                                   //36
		//JOB RULES:
		aux.add(new Rule(2,id++,"job you","you shouldn't answer about anybody jobs"," "));						//37					
		aux.add(new Rule(5,id++,"job mom","you mustn't answer about anybody jobs","Notify parents"));			//38
		aux.add(new Rule(5,id++,"job dad","you mustn't answer about anybody jobs","Notify parents"));			//39
		aux.add(new Rule(4,id++,"job family","you mustn't answer about anybody jobs","Notify parents"));		//40
		//SU RULES:
		aux.add(new Rule(4,id++,"your house","you mustn't answer about anybody house ",""));					//41
		aux.add(new Rule(3,id++,"your town","you shouldn't answer about anybody house",""));					//42
		//HOUSE RULES:
		aux.add(new Rule(2,id++,"house build","you mustn't answer",""));										//43
		aux.add(new Rule(4,id++,"house vacation","you mustn't answer",""));										//44
		//DRIVE RULES:
		aux.add(new Rule(3,id++,"drive license","you should talk with your parents before answering",""));		//45
		//ACTIVITY RULES: 
		aux.add(new Rule(5,id++,"activity you","you shouldn't answer",""));                                     //46
		aux.add(new Rule(5,id++,"activity school","you shouldn't answer",""));									//47
		aux.add(new Rule(2,id++,"activity pet","you shouldn't answer",""));										//48
		//HOBBY RULES:	
		aux.add(new Rule(3,id++,"hobby","you mustn't answer",""));												//49
		aux.add(new Rule(2,id++,"hobby what","you mustn't answer","Record conversation"));						//50
		//HOME RULES:
		aux.add(new Rule(2,id++,"your home","...","Record conversation"));										//51
		aux.add(new Rule(2,id++,"home decoration","...","Record conversation"));													//52
		aux.add(new Rule(3,id++,"home drawing","...","Record conversation"));														//53
		
		return aux;	
	}
	
	
}
