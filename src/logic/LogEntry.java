package logic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LogEntry {
	private static int numberOfLogs = 0;
	private int logEntry;
	private Date logDate;
	private String inputText;
	private List<String> outputPreprocesamiento;
	private List<Rule> outputCotejo;
	List<Criteria> criterio;
	private Rule outputResolucionConflicto;
	
	
	public LogEntry() {
		logEntry = numberOfLogs;
		numberOfLogs++;
		logDate = new Date();
	}


	public void setInputText(String inputText) {
		this.inputText = inputText;
	}


	public void setOutputPreprocesamiento(List<String> outputPreprocesamiento) {
		this.outputPreprocesamiento = outputPreprocesamiento;
	}


	public void setOutputCotejo(List<Rule> outputCotejo) {
		this.outputCotejo = outputCotejo;
	}


	public void setCriterio(List<Criteria> criterio) {
		this.criterio = criterio;
	}


	public void setOutputResolucionConflicto(Rule outputResolucionConflicto) {
		this.outputResolucionConflicto = outputResolucionConflicto;
	}
	
	@Override
	public String toString() {
		
		StringBuilder opstring = new StringBuilder();
		
		for(String s : this.outputPreprocesamiento) {
			opstring.append(s+" ");
		}
		
		StringBuilder ocstring = new StringBuilder();
		
		for(Rule r : this.outputCotejo) {
			ocstring.append(r.toString() + "\r\n");
		}
		
		StringBuilder criteriastring = new StringBuilder();
		
		for(Criteria r : this.criterio) {
			criteriastring.append(r.toString()+", ");
		}

		if(this.outputPreprocesamiento.size() > 0) {
			if(this.outputCotejo.size() > 0) {
				return "Entrada n�mero: " + this.logEntry + " con fecha: " + this.logDate + "\r\n"+
						"Se recibi� el siguiente texto:\r\n" + this.inputText +"\r\n"+
						"Luego de procesar el texto se obtuvo:\r\n"+opstring.toString()+"\r\n"+
						"Se activaron las siguientes reglas:\r\n" + ocstring.toString()+"\r\n"+
						"Utilizando los siguientes criterios:\r\n" + criteriastring.toString()+"\r\n"+
						"Se decidi� por la regla n� " + this.outputResolucionConflicto.ruleNumber()+"\r\n"+
						"------------------------------------------------------\r\n";
			}else{
				return "Entrada n�mero: " + this.logEntry + " con fecha: " + this.logDate + "\r\n"+
						"Se recibi� el siguiente texto:\r\n" + this.inputText +
						"Luego de procesar el texto se obtuvo:\r\n"+opstring.toString()+"\r\n"+
						"No se activaron ninguna reglas.\r\n"+
						"------------------------------------------------------\r\n";
			}
		}else {
			return "Entrada n�mero: " + this.logEntry + " con fecha: " + this.logDate + "\r\n"+
					"Se recibi� el siguiente texto:\r\n" + this.inputText +
					"No se encontraron palabras claves.\r\n"+
					"------------------------------------------------------\r\n";
		}
		
	}
}
